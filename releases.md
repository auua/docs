#### Latest Release &ndash;&nbsp; `v1.8.0-rc1`

 - `[commento-v1.8.0-rc1-linux-glibc-amd64.tar.gz](https://dl.commento.io/release/commento-v1.8.0-rc1-linux-glibc-amd64.tar.gz) ([signature](https://dl.commento.io/release/commento-v1.8.0-rc1-linux-glibc-amd64.tar.gz.asc))`  
   <p class="sha">dfaffe4f765e34ce46f61633f4dc3fc7044784bbd3eb4a2b63f6013e6b1cc84a</p>

 - `[commento-v1.8.0-rc1-src.tar.gz](https://dl.commento.io/release/commento-v1.8.0-rc1-src.tar.gz)`  
   <p class="sha">4c7f89271df7cd3b055b4fd0dc69c9f141e12e2f016b6f722a6fcdd0ceead631</p>

#### Previous Releases

##### `v1.7.0`

 - No release binaries available for this release.

 - `[commento-v1.7.0-src.tar.gz](https://dl.commento.io/release/commento-v1.7.0-src.tar.gz)`  
   <p class="sha">65396c85358b16d53aec545feefc31a7379d70ace796da27d6133f2821137fc7</p>

##### `v1.6.2`

 - No release binaries available for this release.

 - `[commento-v1.6.2-src.tar.gz](https://dl.commento.io/release/commento-v1.6.2-src.tar.gz)`  
   <p class="sha">28728b24c6b5c19dce492cc751f48193129ec1160472abcdaac599719362fa61</p>

##### `v1.5.0`

 - No release binaries available for this release.

 - `[commento-v1.5.0-src.tar.gz](https://dl.commento.io/release/commento-v1.5.0-src.tar.gz)`  
   <p class="sha">c9af6d58a24c0960be33a23c283e1048e4a67e89417f578222e7c9ffe465051e</p>

##### `v1.4.2`

 - No release binaries available for this release.

 - `[commento-v1.4.2-src.tar.gz](https://dl.commento.io/release/commento-v1.4.2-src.tar.gz)`  
   <p class="sha">f1c847c6c4cc3243fe8341795850ed9c34332729a29328733f7687266c05ca51</p>

##### `v1.3.1`

 - No release binaries available for this release.

 - `[commento-v1.3.1-src.tar.gz](https://dl.commento.io/release/commento-v1.3.1-src.tar.gz)`  
   <p class="sha">38900427282f07b5f37c6fe853724e6d741d4356a9c4b3193cbff4a6595808a4</p>

##### `v1.2.0`

 - No release binaries available for this release.

 - `[commento-v1.2.0-src.tar.gz](https://dl.commento.io/release/commento-v1.2.0-src.tar.gz)`  
   <p class="sha">e41335e86addac4c7c9f31803844fbc42555feb0341e6eb137dc463f5fdb04b3</p>

##### `v1.1.3`

 - No release binaries available for this release.

 - `[commento-v1.1.3-src.tar.gz](https://dl.commento.io/release/commento-v1.1.3-src.tar.gz)`  
   <p class="sha">8fe889305d8737b06190d820cc49983152f348198e57d9741d426fa58acbed0c</p>

##### `v1.0.0`

 - No release binaries available for this release.

 - `[commento-v1.0.0-src.tar.gz](https://dl.commento.io/release/commento-v1.0.0-src.tar.gz)`  
   <p class="sha">4b3409efdf9ffeae75366539ac0557b35ab9d106718454bbada5c2d2fe3dabc8</p>

