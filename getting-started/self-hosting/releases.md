### Commento Releases

Commento uses [semantic versioning](https://semver.org) to denote releases. The current major version family is `1.x.y` and the project is considered to be stable and there won't be any breaking changes across releases. It is highly recommended you keep your installation updated for security and performance reasons; Commento will notify you in the logs when a new release is made.

If you download a binary release for your system, please follow the [binary installation instructions](installation/on-your-server/release-binaries.md).

If you are building from source, follow the [source installation instructions](installation/on-your-server/compiling-source.md).

{% include "../../releases.md" %}
